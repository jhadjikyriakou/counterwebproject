package com.qaagility.controller;

public class Counting {

    public int division(int a, int b) {
        if (b == 0) {
            return Integer.MAX_VALUE;
        } else {
            return a / b;
        }
    }

}
